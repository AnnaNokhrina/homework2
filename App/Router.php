<?php

namespace Anna\Php2\App;

/** сопоставляет имена и контроллеры */
class Router
{
    public function run(string $url)
    {
        $urlInfo = parse_url(urldecode($url));

        $path = "";

        if (isset($urlInfo["path"])) {
            $path = trim($urlInfo["path"]);
        }

        $path = trim($path, '/');

        if (!empty($path)) {
            $path = explode('/', $path);

            if (isset($path[0])) {
                $controller = ucfirst(trim($path[0]));
            }

            if (isset($path[1])) {
                $action = ucfirst(trim($path[1]));
            }
        }

        if (empty($action)) {
            $action = 'Articles';
        }

        if (empty($controller)) {
            $controller = 'Site';
        }

        $nameWithNameSpace = '\Anna\Php2\App\Controllers\\' . $controller . "Controller";

        if (!class_exists($nameWithNameSpace)) {
            throw new \Exception("Страница не найдена", 404);
        }
        $ctrl = new $nameWithNameSpace();

        $actionName = 'action' . $action;
        if (!method_exists($ctrl, $actionName)) {
            throw new \Exception("Страница не найдена", 404);
        }

        if ($ctrl->checkAccess($action)) {
            $ctrl->{$actionName}();
        } else {
            throw new \Exception("Нет прав", 403);
        }

    }
}
