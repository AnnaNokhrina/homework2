<?php
use Anna\Php2\App\View;
use Anna\Php2\App\Models\Article;


require_once __DIR__ . '/autoload.php';
ini_set('display_errors', 1);

$view = new View();
$view->article = Article::findById( (int)$_GET['id']);
echo $view->render( 'edit');