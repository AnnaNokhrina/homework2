<?php


namespace Anna\Php2\App;


class FormHelper
{
    public static function printErrors(string $fieldName, ErrorsValidate $validator)
    {
        if ($validator->hasError($fieldName)) {
            echo "<ol>";
            foreach ($validator->getError('content') as $error) {
                echo "<li>{$error->getMessage()}</li>";
            }
            echo "</ol>";
        }
    }

}