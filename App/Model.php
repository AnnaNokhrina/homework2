<?php

namespace Anna\Php2\App;

/**
 * Абстрактный класс таблицы в бд.
 *
 * В классе описаны основные методы работы с таблицами:
 * метод получения всех записей, получения определенной записи из таблицы,
 * вставки, обновления, сохранения, удаления записи в таблице,
 * поиска записи таблице
 *
 * заданное сообщение скидывает в текстовый файл
 *
 */
abstract class Model
{
    public $id;

    /**
     * Определяет имя таблицы в бд
     *
     * @return string
     */
    abstract public static function getTableName(): string;

    /**
     * Возвращает все записи в таблице
     *
     * @return array
     * @throws DbException
     */
    public static function findAll(): array
    {
        $db = Db::getDbConnection();
        return $db->query(static::class, "SELECT * FROM " . static::getTableName(), []);
    }

    /**
     * Возвращает выбранную $id запись
     *
     * @param int $id
     * @return object|null
     * @throws DbException
     */
    public static function findById(int $id): ?object
    {
        $db = Db::getDbConnection();
        $res = $db->queryOne("SELECT * FROM " . static::getTableName() . " WHERE id =:id", static::class,
            [':id' => $id]);
        if ($res) {
            return $res;
        }
        return null;
    }

    /**
     * Решает "новая" модель или нет и, в зависимости от этого,
     * Вызывает либо insert(), либо update()
     *
     * @return bool
     * @throws DbException
     */
    public function save(): bool
    {
        if (is_null($this->id)) {
            return $this->insert();
        }
        return $this->update();
    }

    /**
     * Вставляет в таблицу новую запись, основываясь на св-вах объекта
     * После вставки возвращает id вставленной записи
     *
     * @return bool
     * @throws DbException
     */
    public function insert(): bool
    {
        $cols = [];
        $data = [];

        foreach ($this as $name => $value) {
            if ('id' == $name || 'author_id' == $name || '_author' == $name) {
                continue;
            }
            $cols[] = $name;
            $data[':' . $name] = $value;
        }

        $sql = '
    INSERT INTO ' . static::getTableName() . '  (' . implode(', ', $cols) . ')
    VALUES (' . implode(', ', array_keys($data)) . ')';

        $db = Db::getDbConnection();
        return $db->execute($sql, $data);
    }

    /**
     * Обновляет в таблице выбранную запись, основываясь на св-вах объекта
     *
     * @return bool
     * @throws DbException
     */
    public function update(): bool
    {
        $data = [];
        $cols = [];

        foreach ($this as $key => $value) {
            if ('author_id' == $key || '_author' == $key) {
                continue;
            }
            $data[':' . $key] = $value;
            if ('id' == $key) {
                continue;
            }
            $cols[] = $key . ' = :' . $key;
        }

        $sql = 'UPDATE ' . static::getTableName() . ' SET ' . implode(', ', $cols) . ' WHERE id = :id';

        $db = Db::getDbConnection();
        return $db->execute($sql, $data);
    }

    /**
     * Удаляет запись из таблицы
     *
     * @return bool
     * @throws DbException
     */
    public function delete(): bool
    {
        $db = Db::getDbConnection();
        return $db->execute(
            'DELETE FROM ' . static::getTableName() . ' WHERE id = :id',
            [':id' => $this->id]
        );
    }

    /**
     * Проверяет, есть ли запись с данным id в таблице
     *
     * @return bool
     * @throws DbException
     */
    public function existInDb(): bool
    {
        $db = Db::getDbConnection();
        return !empty($db->query(static::class, 'SELECT * FROM ' . static::getTableName() .
            ' WHERE id = :id', [':id' => $this->id]));
    }

    public function isNewRecord(): bool
    {
        return null === $this->id;
    }
}