<?php

namespace Anna\Php2\App;

/**
 * Класс-логгер
 * Записывает в текстовый лог информацию об ошибках - когда и где возникла ошибка, какая
 */
class Logger
{
    /** @var $flHandler , обработчик файла логгирования */
    private $flHandler;

    /** @var string путь к папке с логами */
    private $fdir = __DIR__ . '/../log/';

    /** @var string имя файла, где ведется запись */
    private $fname = '/log.txt';

    /**
     * @param $line, информация об ошибке
     */
    public function write(string $line) : void
    {
        $this->start();

        if ($this->check()) {
            fwrite($this->flHandler, date("Y-m-d h:i:s e") . $line . "\n");
        }
        $this->stop();
    }

    public function check(): bool
    {
        $files = scandir($this->fdir);
        $num = count($files) - 2;

        if ($this->getLimit()) {
            rename($this->fdir . $this->fname, $this->fdir . 'log_' . $num . '.txt');
        }
        return true;
    }

    /**
     * Завершает работу с $flHandler
     * Закрывает файл для работы
     */
    public function stop() : void
    {
        fclose($this->flHandler);
    }

    /**
     * @param int $limit
     * @return bool, если лимит превышен
     */
    public function getLimit(int $limit = 1048576): bool
    {
        if (filesize($this->flHandler) > $limit) {
            return true;
        }
        return false;
    }

    /**
     * Начало работы с $flHandler
     * Открывает файл $flName для чтения/записи в конце файла
     */
    public function start() : void
    {
        $this->flHandler = fopen($this->fdir . $this->fname, 'a+');
    }




}