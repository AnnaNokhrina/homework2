<?php

namespace Anna\Php2\App;

use PDO;

require_once __DIR__ . '/autoload.php';

/**
 * Класс подключения к базе данных.
 * Является синглтоном.
 *
 * В классе реализованы методы запуска запросов execute(),
 * для выполнения sql-запросов query(), queryOne(),
 * и для получения номера последней всталенной записи в бд
 */
class Db
{
    /** @var PDO $dbh обработчик бд */
    protected $dbh;

    /** @var static */
    private static $instance;


    /**
     * Возвращает объект подключения к бд
     * Является синглтоном
     *
     * @return Db
     * @throws DbException
     */
    public static function getDbConnection(): Db
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * Db constructor, выполняет соединение между php и бд
     * @throws DbException, нет соединения с бд
     */
    public function __construct()
    {
        try {
            $config = Config::getInstance();

            $this->dbh = new PDO(
                $config->data['db']['dsn'],
                $config->data['db']['username'],
                $config->data['db']['password'],
                $config->data['db']['options']
            );
        } catch (\PDOException $error) {
            throw new DbException('', 'Ошибка подключения к БД!');
        }
    }

    /**
     * Запускает подготовленный запрос на выполнение
     *
     * @param string $query
     * @param array $params
     * @return bool
     * @throws DbException
     */
    public function execute(string $query, array $params = []): bool
    {
        $sth = $this->dbh->prepare($query);
        $res = $sth->execute($params);
        if ($res) {
            return $res;
        }
        throw new DbException($query, 'Запрос не может быть выполнен!');
    }

    /**
     * Подготавливает, запускает и выполняет $sql запрос с парметрами $data
     * Возвращает результат запроса в виде массива объектов заданного класса
     *
     * @param $className
     * @param $sql
     * @param array $data
     * @return array|null
     * @throws DbException
     */
    public function query($className, $sql, $data = []): ?array
    {
        $sth = $this->dbh->prepare($sql);
        $res = $sth->execute($data);
        if ($res) {
            return $sth->fetchAll(PDO::FETCH_CLASS, $className) ?? null;
        }
        throw new DbException($sql, 'Запрос не может быть выполнен!');
    }

    /**
     * Подготавливает, запускает и выполняет $sql запрос с парметрами $data
     * Возвращает результат запроса в виде объекта заданного класса
     *
     * @param $sql
     * @param $className
     * @param array $params
     * @return object|null
     * @throws DbException
     */
    public function queryOne($sql, $className, $params = []): ?object
    {
        $data = $this->query($className, $sql, $params);
        return $data[0] ?? null;
    }

    /**
     * Возвращает id последней вставленной записи
     *
     * @return int
     */
    public function getLastId(): int
    {
        return (int)$this->dbh->lastInsertId();
    }
}