<?php

namespace Anna\Php2\App;

use Anna\Php2\App\Models\User;

abstract class Controller
{
    protected $view;
    public $user;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->view = new View($this);
        if (isset($_SESSION['name'])) {
            $this->user = User::findByLogin($_SESSION['name']);
        } else {
            $guest = new User();
            $guest->admin = 0;
            $guest->name = 'guest';
            $this->user = $guest;
        }
    }

    /**
     * Проверяет права доступа на работу вызываемого экшна
     * По умолчанию для всех экшнов всех контроллеров доступ открыт
     *
     * @param string $action
     * @return bool
     */
    public function checkAccess(string $action): bool
    {
        return true;
    }

    public function redirect(string $url)
    {
        header('Location:' . $url);
    }

    /**
     * Вызывает экшн по имени $name и передает аргументы $arguments на вход экшна,
     * где $name - имя экшна в системе ЧПУ вида /XXX/YYY/ZZZ, где XXX - имя контроллера, YYY - имя экшна,
     * ZZZ - аргументы, а $arguments дополнительны аргументы, которые могут переданы на выход экшна (например, id)
     * @param $name
     * @param $arguments
     */
    /*public function __call($name, $arguments)
    {
        var_dump($name);
        if ($this->checkAccess($name)) {
            $this->{'action' . $name}(...$arguments);
        } else {
            die('Нет доступа! ');
        }
    }*/




}