<?php

namespace Anna\Php2\App\Models;

use Anna\Php2\App\Db as Db;
use Anna\Php2\App\DbException;
use Anna\Php2\App\ErrorsValidate;
use Anna\Php2\App\Form;
use Anna\Php2\App\Model as Model;
use Anna\Php2\App\Models\Author as Author;
use Exception;

/**
 * Класс, наследуемый от класса Модель.
 * Хранит в себе информацию об идентификаторе, заголовке, содержании статьи и номер автора статьи.
 *
 * Содержит метод выполнения sql-запросов с парметрами, метод получения автора статьи, получения имени таблицы в бд,
 * метод валидации ошибок, при заполнении статьи, метод возвращения всего массива ошибок,
 * возвращения ошбок с заданным ключом, метод проверки на существование всех ошибок,
 * метод проверки на существование ошибок с заданным ключом
 */
class Article
    extends Model
    implements ErrorsValidate

{
    public $title;
    public $content;
    public $author_id;

    /** @var Author $author */
    protected $_author;

    private $errors = [];

    /**
     * @param $name
     * @return Author
     * @throws DbException
     */
    public function __get($name)
    {
        if ('author' === $name) {
            return $this->getAuthor();
        }
    }

    /**
     * Возвращает имя таблицы в бд
     *
     * @return string
     */
    public static function getTableName(): string
    {
        return 'news';
    }

    /**
     * Выполняет запрос с параметрами string $paramsSql (например, ...order by column_name limit 100)
     * Возвращает массив результата запроса
     *
     * @param string $paramsSql
     * @return array|null
     * @throws DbException
     */
    public static function findBySql(string $paramsSql): ?array
    {
        $db = Db::getDbConnection();
        return $db->query(
            "SELECT * FROM " . static::getTableName() . ' ' . $paramsSql, static::class, []);
    }

    /**
     * Возвращает имя автора из таблицы authors с помощью связи по authors_id
     *
     * @return Author
     * @throws DbException
     */
    public function getAuthor(): Author
    {
        $db = Db::getDbConnection();
        if (empty($this->_author)) {
            $this->_author = $db->queryOne(
                "SELECT * FROM authors WHERE id = :id", 'Anna\Php2\App\Models\Author',
                [':id' => $this->author_id]
            );
        }
        return $this->_author;
    }

    /**
     * Возвращает информацию о наличии ошибок при заполнении статьи
     *
     * @return bool
     */
    public function validate(): bool
    {
        $this->errors = [];

        $this->title = trim($this->title);
        $this->content = trim($this->content);

        if (empty($this->title)) {
            $this->errors['title'][] = new Exception('Необходимо заполнить поле title');
        }

        if (empty($this->content)) {
            $this->errors['content'][] = new Exception('Необходимо заполнить поле content');
        }

        if (strlen($this->title) > 100) {
            $this->errors['title'][] = new Exception('Превышено максимальное количество символов');
        }

        if (strlen($this->content) > 1000) {
            $this->errors['content'][] = new Exception('Превышено максимальное количество символов');
        }

        return empty($this->errors);
    }

    /**
     * Возвращает массив ошибок this->errors
     *
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Проверяет наличие ошибок в this->errors
     *
     * @return bool
     */
    public function hasErrors(): bool
    {
        return !empty($this->getErrors());
    }

    /**
     * Проверяет наличие ошибок в this->errors по ключу $name
     *
     * @param string $name
     * @return bool
     */
    public function hasError(string $name) : bool
    {
        return !empty($this->errors[$name]);
    }

    /**
     * Возвращает массив ошибок this->errors[$name] с ключом $name
     *
     * @param string $name
     * @return array
     */
    public function getError(string $name) : array
    {
        if ($this->hasError($name)) {
            return $this->errors[$name];
        }
    }

}