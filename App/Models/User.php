<?php

namespace Anna\Php2\App\Models;

use Anna\Php2\App\Db;
use Anna\Php2\App\DbException;
use Anna\Php2\App\Model;

/**
 * Класс Пользователь, наследуемый от класса Модель.
 * Хранит в себе имя, почту и номер пользователя в бд.
 *
 * Есть метод для определения имени таблицы в бд и проверки прав администратора, метод поиска пользователя по логину,
 * метод проверки прав администратора
 */
class User extends Model
{
    public $email;
    public $name;
    public $admin;


    /**
     * Заполняет св-ва модели данными из массива $data
     * Паттерн Мультиисключение
     *
     * @param array $data
     */
    public function fill(array $data)
    {
//                return empty($val) ? new \Exception("Пустое значение") : null;

        $validators = [

            'empty' => function ($val): ?\Exception {

                if (empty($val)) {
                    return new \Exception("Пустое значение");
                }
                return null;
            },

            'email' => function ($val): ?\Exception {

                if (empty(strstr($val, '@'))) {
                    return new \Exception('Введенный email "' . $val . '" не прошел проверку!');
                }
                return null;
            },

            'admin' => function ($val): ?\Exception {
                $vals = [0, 1];

                if (empty(in_array((int)$val, $vals))) {
                    return new \Exception('Несуществующее значение для поля "' . '"! Значение: "' . $val . '"');
                }
                return null;
            },

            'name' => function ($val): ?\Exception {
                if (strlen($val) < 5) {
                    return new \Exception('Слишком короткое значение для поля! Значение: "' . $val . '"');
                }
                return null;
            }

        ];
    }

    /**
     * Определяет имя таблицы в бд
     *
     * @return string
     */
    public static function getTableName(): string
    {
        return 'user';
    }

    /**
     * Возвращает все св-ва пользователя с искомым логином $login
     *
     * @param string $login
     * @return User|null
     * @throws DbException
     */
    public static function findByLogin(string $login): ?User
    {
        $db = Db::getDbConnection();

        $res = $db->queryOne("SELECT * FROM " . static::getTableName() . " WHERE name =:login",
            static::class, [':login' => $login]);

        return $res;
    }

    /**
     * Проверяет, являяется ли пользователь администратором
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->admin == 1;
    }
}