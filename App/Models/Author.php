<?php

namespace Anna\Php2\App\Models;
use Anna\Php2\App\Errors;
use Anna\Php2\App\Model;
use Exception;

/**
 * Класс Автор, наследуемый от класса Модель.
 * Хранит в себе информацию об имени и номере автора.
 *
 * Есть метод для определения имени таблицы в бд.
 */
class Author extends Model
{
    public $name;

    /**
     * Определяет имя таблицы в бд
     * @return string
     */
    public static function getTableName() : string
    {
        return 'authors';
    }
}