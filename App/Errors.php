<?php

namespace Anna\Php2\App;

use Exception;

class Errors extends Exception
{
    protected $errors;

    /**
     * Добавляет новое исключение $e в массив исключений $errors[]
     *
     * @param Exception $e
     */
    public function add(Exception $e) : void
    {
        $this->errors[] = $e;
    }

    /**
     * Возвращает массив исключений $errors[]
     *
     * @return array
     */
    public function getAll() : array
    {
        return $this->errors;
    }

    /**
     * Проверяет масив на наличие исключений
     *
     * @return bool
     */
    public function isEmpty() : bool
    {
        return empty($this->errors);
    }
}