<?php

namespace Anna\Php2\App;

/**
 * Класс подключения к бд.
 * Является синглтоном.
 * Создает и запоминает файл конфига.
 */
class Config
{
    public $data;

    /** объект конфига */
    private static $instance;

    /**
     * выполняет чтение файла конфигурации
     * Config constructor
     */
    protected function __construct()
    {
        $this->data = require __DIR__ . '/config_db.php';
    }

    /**
     * Возвращает объект конфига.
     * Является синглтоном.
     *
     * @return Config
     */
    public static function getInstance() : Config
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }
}