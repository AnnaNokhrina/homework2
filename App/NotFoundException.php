<?php

namespace Anna\Php2\App;

use Throwable;

/**
 * Класс исключений, возникающих в том случае, когда невозможно найти в бд запрашиваемую запись
 */
class NotFoundException extends \Exception
{
    private $id;

    /**
     * id записи, которая отсутствует в базе
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct($id, $message = "Ошибка 404 - Не найдено", $code = 0, Throwable $previous = null)
    {
        $this->id = $id;
        parent::__construct($message, $code, $previous);
    }
}