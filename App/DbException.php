<?php

namespace Anna\Php2\App;
use Throwable;

/**
 * Класс исключений, возникающих в процессе работы с бд
 */
class DbException extends \Exception
{
    protected $query;


    public function __construct($query = "", $message = "", $code = 0, Throwable $previous = null)
    {
        $this->query = $query;
        parent::__construct($message, $code, $previous);
    }
    public function getQuery()
    {
        return $this->query;
    }
}