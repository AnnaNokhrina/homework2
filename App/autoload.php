<?php

function __autoload(string $className)
{
    $className = str_replace('Anna\\', 'www/', $className);
    $fileName = '/' . str_replace('\\', '/', $className) . '.php';

    if (file_exists($fileName)) {
        require $fileName;
    }

}