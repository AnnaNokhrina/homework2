<?php

namespace Anna\Php2\App;

/**
 * Класс для отображения шаблонов.
 * Хранит в себе данные для отображения и методы подключения и отображеня и содержимого шаблона.
 * В классе реализован пример простейшего итератора.
 */
class View
{
    protected $data = [];
    private $controller;

    public function __construct(?Controller $controller)
    {
        $this->controller = $controller;
    }

    /**
     * Подключает шаблон для отображения
     *
     * @param string $template
     * @return string
     */
    public function render(string $template): string
    {
        ob_start();
        include '/www/Php2/Templates/' . $template . '.php';
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }
}