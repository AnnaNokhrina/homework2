<?php

namespace Anna\Php2\App\Controllers;

use Anna\Php2\App\Controller;
use Anna\Php2\App\DbException;
use Anna\Php2\App\Models\Article;
use Exception;

class ArticleController extends Controller
{
    /**
     * Action отображения одной новости
     *
     * @throws DbException
     * @throws Exception
     */
    public function actionArticle(): void
    {
        if (empty($_GET['id'])) {
            throw new Exception('Неверный запрос', 400);
        }

        $id = (int)$_GET['id'];
        $article = Article::findById($id);

        if (null === $article) {
            throw new Exception('Запись не найдена', 404);
        }

        $this->view->article = $article;
        echo $this->view->render('article');
    }

    /**
     * Action удаления новости
     * Удаляет выбранную новость из бд
     *
     * @throws DbException
     * @throws Exception
     */
    public function actionDelete(): void
    {
        if (empty($_GET['id'])) {
            throw new Exception('Неверный запрос', 400);
        }

        $id = (int)$_GET['id'];
        $article = Article::findById($id);

        if (null === $article) {
            throw new Exception('Запись не найдена', 404);
        }
        $article->delete();
        $this->redirect('/');
    }

    /**
     * Action редактирования новости
     * Обновляет информацию о выбранной новости в соответствие с переданными св-вами
     *
     * @throws DbException
     * @throws Exception
     */
    public function actionUpdate(): void
    {
        if (empty($_GET['id'])) {
            throw new Exception('Неверный запрос', 400);
        }

        $id = (int)$_GET['id'];
        $article = Article::findById($id);

        if (null === $article) {
            throw new Exception('Запись не найдена', 404);
        }
        $this->view->article = $article;

        if (!empty($_POST)) {
            $article->title = $_POST['title'] ?? '';
            $article->content = $_POST['content'] ?? '';

            if ($article->validate() && $article->save()) {
                $this->redirect('/');
            }
        }

        echo $this->view->render('edit');
    }

    /**
     * Создает новость, заполняет ее в соответствие с переданными св-вами, сохраняет в бд
     *
     * @throws DbException
     */
    public function actionCreate(): void
    {
        $article = new Article();

        if (!empty($_POST)) {
            $article->title = $_POST['title'] ?? '';
            $article->content = $_POST['content'] ?? '';

            if ($article->validate() && $article->save()) {
                $this->redirect('/');
            }
        }

        $this->view->article = $article;
        echo $this->view->render('edit');
    }

    /**
     * Сохраняет новость в бд
     *
     * @return Article
     * @throws DbException
     */
    private function save(): Article
    {
        $article = new Article();
        foreach ($_POST as $key => $value) {
            $article->$key = $value;
        }

        if ($article->validate()) {
            $article->save();
        }

        return $article;
    }
}