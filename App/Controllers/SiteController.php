<?php

namespace Anna\Php2\App\Controllers;

use Anna\Php2\App\Controller;
use Anna\Php2\App\DbException;
use Anna\Php2\App\Models\Article;
use Anna\Php2\App\Models\User;

class SiteController extends Controller
{
    /**
     * Отображает все новости
     *
     * @throws DbException
     */
    public function actionArticles()
    {
        $this->view->articles = Article::findAll();
        echo $this->view->render('site');
    }

    /**
     * Проверяет, существует ли пользователь в базе
     *
     * @throws DbException
     */
    public function actionLogin()
    {
        if (!empty($_POST['login'])) {

            $login = trim($_POST['login']);
            $user = User::findByLogin($login);

            if ($user) {
                $_SESSION['name'] = $user->name;
            } else {
                $this->view->errorMessage = 'Не найден пользователь с именем "' . $_POST['login'] . '"!';

                echo $this->view->render('login');
                return;
            }
            $this->redirect('/');
        }
        echo $this->view->render('login');
    }

    /**
     * Уничтожает все данные сессии, редиректит на главную страницу
     */
    public function actionLogout()
    {
        unset($_SESSION['admin']);
        session_destroy();
        $this->redirect('/');
    }
}
