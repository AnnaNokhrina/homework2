<?php


namespace Anna\Php2\App;


interface ErrorsValidate
{
    /**
     * Проверяет наличие ошибок у поля
     *
     * @param string $name
     * @return bool
     */
    public function hasError(string $name) : bool;

    /**
     * Возвращает массив ошибок поля
     *
     * @param string $name
     * @return array
     */
    public function getError(string $name) : array;
}