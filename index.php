<?php

ini_set('display_errors', 1);
require_once __DIR__ . '/App/autoload.php';

use Anna\Php2\App\DbException;
use Anna\Php2\App\Errors;
use Anna\Php2\App\Models\Article;
use Anna\Php2\App\Models\Author;
use Anna\Php2\App\Router;
use Anna\Php2\App\View;

session_start();

//$req = $_SERVER["REQUEST_URI"];
//$uri = explode('/', trim($req, '/'));

//$ctrl = ucfirst($uri[0]) ?? 'Site';
//$act = ucfirst($uri[1]) ?? 'Site';


$router = new Router();

try {
    $router->run($_SERVER["REQUEST_URI"]);
} catch (Exception $e) {
    $errorView = new View(null);
    $errorView->errorMessage = $e->getMessage();
    $errorView->errorCode = $e->getCode() == 0 ? 500 : $e->getCode();

    echo $errorView->render('error');

}
//
//$logger = new Logger();
//$logger->start();


//
//$class = '\Anna\Php2\App\Controllers\Users\\' . $ctrl;
//if (isset($_SESSION['name'])) {
//    $class = '\Anna\Php2\App\Controllers\Admin\\' . $ctrl;
//}


//$ctrl = new $class();
//
//$args = [];
//foreach ($uri as $name => $value) {
//    if ('0' == $name || '1' == $name) {
//        continue;
//    }
//    $args[] = ucfirst($value);
//}
//
//try {
//    $ctrl->$act(...$args);
//} catch (DbException $dbError) {
//    echo 'Ошибка в бд при выполнении запроса: "' . $dbError->getQuery() . '". ' . $dbError->getMessage();
//    die;
//}