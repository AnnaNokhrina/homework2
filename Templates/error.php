<?php /** @var Anna\Php2\App\View $this */ ?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP</title>
    <style type="text/css">
        body {
            text-align: center;
            font-family: Geneva, Arial, Helvetica, sans-serif;
        }

        h1 {
            margin-top: 50px;
        }
    </style>
</head>
<body>

<h1>Error <?= $this->errorCode; ?></h1>
<?= $this->errorMessage; ?>
</body>
</html>