<?php /** @var Anna\Php2\App\View $this */ ?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP</title>
    <style>
        body {
            font-family: Geneva, Arial, Helvetica, sans-serif;
        }

        table {
            width: 100%;
            margin-top: 20px;
        }

        thead {
            background-color: #abc1d7;
        }

        .gbutton {
            padding: 5px;
            color: white;
            background-color: darkolivegreen;
            border: 1px solid #324422;
            border-radius: 3px;
            text-decoration: none;
            font-size: 0.7em;
        }
    </style>
</head>
<body>

<?php

if ($this->controller->user->isAdmin()) { ?>
    <a href="/site/logout">Выйти</a>

    <form action="/article/create/" method="post">
        <input type="text" placeholder="title" name="title">
        <input type="text" placeholder="content" name="content">
        <button type="submit">Добавить</button>
    </form>

<?php } else { ?>
    <a href="/site/login/">Войти</a>
<?php }

if ($this->controller->user->isAdmin()) { ?>
    <h2>Articles <a href="/article/create" class="gbutton">Создать</a></h2>
<?php } else { ?>
    <h2>Articles</h2>

<?php } ?>
<table>
    <thead>
    <tr>
        <td>ID</td>
        <td>Title</td>
        <?php if ($this->controller->user->isAdmin()) { ?>
            <td>Actions</td>
        <?php } ?>

    </tr>
    </thead>
    <?php foreach ($this->articles as $article) { ?>
        <tr>
            <td><?= $article->id; ?></td>
            <td><a href="/article/article?id=<?= $article->id; ?>"> <?= $article->title; ?> </a></td>
            <td>
                <?php if ($this->controller->user->isAdmin()) { ?>
                    <a href="/article/delete?id=<?= $article->id; ?>">Delete</a>
                    <a href="/article/update?id=<?= $article->id; ?>"> Edit </a>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>

</table>

</body>
</html>