<style type="text/css">
    .field-row {
        padding: 5px;
        margin-bottom: 15px;
    }
    .error {
        color: red;
    }

    .field-row label{
        width: 100px;
        display: inline-block;
        padding-right: 10px;
        text-align: right;
    }

    .field-row input, textarea{
        width: 500px;
    }
</style>
<?php
/** @var Anna\Php2\App\Controllers\ArticleController $this */

/** @var Anna\Php2\App\Models\Article $article */
$article = $this->article;

use Anna\Php2\App\FormHelper; ?>

<?php

if ($article->isNewRecord()) {
    echo "<h3>Создание новости</h3>";
    $postUrl = "/article/create/";
} else {
    echo "<h3>Редактирование новости #{$article->id}</h3>";
    $postUrl = "/article/update?id={$article->id}";
}

?>

<form action="<?= $postUrl ?>" method="post" style="width: 630px;text-align: center">

    <div class="field-row">
        <label>Заголовок</label>
        <input type="text" placeholder="заголовок" name="title" value="<?= $article->title; ?>">
        <div class="error" style="padding-left: 100px;text-align: left;">
            <?php FormHelper::printErrors('title', $article); ?>
        </div>
    </div>

    <div class="field-row">
        <label>Содержание</label>
        <textarea placeholder="текст статьи" name="content"><?= $article->content; ?></textarea>
        <div class="error" style="padding-left: 100px;text-align: left;">
            <?php FormHelper::printErrors('content', $article); ?>
        </div>
    </div>

    <button type="submit" style="padding: 5px 20px;">Сохранить</button>

</form>