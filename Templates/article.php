<?php /** @var Anna\Php2\App\Models\Article $this */ ?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>project 9</title>
</head>

<body>

<h1 align="center">Articles</h1>
    <h2 ><?= $this->article->title; ?></h2>
    <a href="/article/article/<?= $this->article->id; ?>"> <?= $this->article->content; ?> </a>
    <p>Author: <?= $this->article->content; ?> </p>

</body>
</html>