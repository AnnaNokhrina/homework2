<?php
/** iterator example */
ini_set('display_errors', 1);
require __DIR__ . '/App/autoload.php';

$view = new \Anna\Php2\App\View();

$view->__set('foo', 'bar');
$view->__set('baz', 42);


foreach ($view as $key => $value) {
    echo $key . '=>' . $value;
    echo "\n";
}

$view->iterate();